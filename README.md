# Artica WatchDog #

## About ##

At [Artica](http://artica.cc) we do a lot of interactive applications.

We needed a watchdog application sitting on the Windows system tray that was monitoring if all our processes are still running, logging everything, triggering alerts and sending email reports.

We had many different versions of watchdogs for specific projects and clients, so we decided to do a clean rewrite, make it a bit more versatile and open source it to the community. Enjoy! Feel free to fork and submit patches if you find any bugs or develop some new useful functionality.

## Description ##

Everything is configured through an XML which is read when you first launch the application, the default filename is ```WatchDogConfig.xml```

If you want to use another configuration file you can specify it by editing ```WatchDog.exe.config``` line 7:
```<add key="ConfigFile" value="WatchDogConfig.xml" />```

On this XML you list what applications the watchdog will be handling. Launching the watchdog will kill all previous instances of these applications and restart them all. Each application has a configurable timer delay. After a certain initial idle time the watch dog will periodically check if all of the applications are still being executed. If one of them is not, it will kill all instances of these applications and relaunch them.

Everything is logged on ```%AppData%/Watchdog``` in separate files per day.

Errors and Warnings are not only logged but also triggered on Windows Event Viewer.

You can also configure the watchdog to perform status checks on the machine. These can range from pinging specific IPs to 

Besides the basic process monitoring and relaunch, this watchdog can also be configured to do periodic status checks like ping addresses, check for telnet communication, list contents of directories, list all programs running, checking if specific task is listed on the taskscheduler, perform a daily reboot, etc.

Please check the sourcecode for full list of features.

## Run ##

You can download the latest Windows binary from ```https://bitbucket.org/artica/artica_watchdog/downloads/```

It needs administrator priviliges to run.

## Compile ##

To compile your own version you need Windows and Visual Studio 2015. It's in C#.

## Example Configurations ##

The basic configuration below performs the following actions:

* does not send any notification emails
* when needing to restart the monitored applications, kills them all, waits 5 seconds and only then restarts them
* will reboot after 3 AM once a day
* waits 10 seconds after watchdog was launched before it first checks if all monitored applications are running
* rechecks if monitored applications are running every 60 seconds
* waits 0 seconds after watchdog was launched before it first checks any computer status
* rechecks computer status every 5 seconds
* will not monitor the memory of the processes
* is monitoring application identified on process name as mspaint
* launched mspaint after waiting 5 seconds
* added no arguments to the launch command
* executed the filename ```C:\Windows\System32\mspaint.exe``` when launching
* launched it using normal window style
* launched it without forcing always on top
* launched it as operating on ```C:\Windows\System32``` directory
* allows mspaint to use maximum 10 Gb of RAM before killing it

``` 
<?xml version="1.0" encoding="utf-8"?>
<WatchDog SendEmails="false" IdleTimeAfterRestart="5000" RebootTime="03:00:00" ApplicationCheckDueTime="10000" ApplicationCheckPeriod="60000" StatusCheckDueTime="0" StatusCheckPeriod="5000" MonitorProcessMemory="false">
  <Application FriendlyName="mspaint" TimeUntilProcessStart="5000" Arguments="" FileName="C:\Windows\System32\mspaint.exe" WindowStyle="Normal" TopMost="false" WorkingDirectory="C:\Windows\System32" MaximumSizeInBytes="10000000" />
</WatchDog>
```

Advanced Example:

```
<?xml version="1.0" encoding="utf-8"?>
<WatchDog SendEmails="true" ReceivingMail="receive@mailinator.com" SendingMailServer="smtp.gmail.com" SendingMailPort="587" SendingMailAddress="XXXXXXXX@gmail.com" SendingMailPassword="XXXXXXXX" IdleTimeAfterRestart="5000" RebootTime="03:00:00" ApplicationCheckDueTime="10000" ApplicationCheckPeriod="60000" StatusCheckDueTime="0" StatusCheckPeriod="5000" MonitorProcessMemory="false">
  <Application FriendlyName="mspaint" TimeUntilProcessStart="5000" Arguments="" FileName="C:\Windows\System32\mspaint.exe" WindowStyle="Normal" TopMost="false" WorkingDirectory="C:\Windows\System32" MaximumSizeInBytes="10000000" />
  <StatusCheck Periodicity="120000" PingIP="127.0.0.1" ListProgramsRunning="true" ListDirectoryContents="true" ListDirectoryContentsPath="C:\Users" Uptime="true" TaskSchedulerCheck="true" TaskSchedulerCheckName="AppleSoftwareUpdate" TelnetCheck="true" TelnetIP="127.0.0.1" TelnetMessage="1" TelnetExpectedResponse="PC 1 Input" />
</WatchDog>
```

This more advanced example sends notification emails whenever the watchdog starts or detects one of the monitored applications is down.

It also performs status checks every 2 minutes, creating status.json files on ```%AppData%/Watchdog```. It is configured to ping localhost, list all the programs currently running, list directory contents of ```C:\Users```, list the computer uptime, check if ```AppleSoftwareUpdate``` is listed on the task scheduler, and try to telnet localhost sending the string ```1``` and expecting a response of ```PC 1 Input```.

You can add multiple statuscheck lines performing at different time frames. In case for example you want to check the directory lists of a certain folder every hour, but check if a certain computer pings every 5 minutes.
 
For full list of existing XML arguments and their default configurations please check the source code.