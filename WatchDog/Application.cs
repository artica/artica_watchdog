﻿using System.Diagnostics;
using System.Xml.Serialization;

/// <summary>
/// The WatchDog namespace.
/// </summary>
namespace WatchDog
{
    /// <summary>
    /// Class Application.
    /// </summary>
    [XmlType]
    public class Application
    {
        /// <summary>
        /// The friendly name
        /// </summary>
        [XmlAttribute]
        public string FriendlyName;

        /// <summary>
        /// The time until process start
        /// </summary>
        [XmlAttribute]
        public int TimeUntilProcessStart;

        /// <summary>
        /// The time until process start
        /// </summary>
        [XmlAttribute]
        public long MaximumSizeInBytes;

        /// <summary>
        /// The arguments
        /// </summary>
        [XmlAttribute]
        public string Arguments = "";

        /// <summary>
        /// The file name
        /// </summary>
        [XmlAttribute]
        public string FileName = "";

        /// <summary>
        /// The window style
        /// </summary>
        [XmlAttribute]
        public ProcessWindowStyle WindowStyle = ProcessWindowStyle.Normal;

        /// <summary>
        /// Maximizes and top most Window
        /// </summary>
        [XmlAttribute]
        public bool TopMost = false;

        [XmlAttribute]
        public string WindowName = "";

        /// <summary>
        /// The working directory
        /// </summary>
        [XmlAttribute]
        public string WorkingDirectory = "";

        /// <summary>
        /// Initializes a new instance of the <see cref="Application"/> class.
        /// </summary>
        public Application()
        {
        }
    }
}
