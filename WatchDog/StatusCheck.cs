﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Management;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using TaskScheduler;

namespace WatchDog
{

    /// <summary>
    /// Class StatusCheck
    /// </summary>
    [XmlType]
    public class StatusCheck
    {
        /// <summary>
        /// The periodicity (in seconds) of the status check
        /// </summary>
        [XmlAttribute]
        public int Periodicity;

        /// <summary>
        /// The IP to ping
        /// </summary>
        [XmlAttribute]
        public string PingIP;

        /// <summary>
        /// The boolean state if we should list all programs running or not
        /// </summary>
        [XmlAttribute]
        public bool ListProgramsRunning;

        /// <summary>
        /// The boolean state if we should list content of directory
        /// </summary>
        [XmlAttribute]
        public bool ListDirectoryContents = false;

        /// <summary>
        /// The path of the directory to list contents from
        /// </summary>
        [XmlAttribute]
        public string ListDirectoryContentsPath;

        /// <summary>
        /// The boolean state if we should check the uptime
        /// </summary>
        [XmlAttribute]
        public bool Uptime = false;

        /// <summary>
        /// The boolean state if we should check if a specific task is listed on the task scheduler
        /// </summary>
        [XmlAttribute]
        public bool TaskSchedulerCheck = true;

        /// <summary>
        /// The name of the task to check on the task scheduler
        /// </summary>
        [XmlAttribute]
        public string TaskSchedulerCheckName;

        /// <summary>
        /// The boolean state if we should check the response of a specific telnet connection
        /// </summary>
        [XmlAttribute]
        public bool TelnetCheck = false;

        /// <summary>
        /// The IP of the telnet connection to test
        /// </summary>
        [XmlAttribute]
        public string TelnetIP;

        /// <summary>
        /// The message to send via telnet
        /// </summary>
        [XmlAttribute]
        public string TelnetMessage;

        /// <summary>
        /// The message expected to receive via telnet
        /// </summary>
        [XmlAttribute]
        public string TelnetExpectedResponse;

        private DateTime LastActivation;

        /// <summary>
        /// Constructor
        /// </summary>
        public StatusCheck()
        {
            LastActivation = DateTime.Now;
        }

        /// <summary>
        /// Gets a List of the programs Running in a JSon String  format
        /// </summary>
        /// <returns>List of the processes running with process name, process id, start time, total processor time</returns>
        public string ProgramsRunning()
        {
            string output = "[";
            //TODO: use a try catch
            Process[] processlist = Process.GetProcesses();

            bool first = true;
            foreach (Process p in processlist)
            {
                string processname = p.ProcessName;
                string pid = (p.Id).ToString();
                string starttime = ""; // p.StartTime;
                string cpu = ""; // p.TotalProcessorTime;
                try
                {
                    starttime = p.StartTime.ToString();
                    cpu = p.TotalProcessorTime.ToString();
                }
                catch (Exception exc)
                {
                    //Report("Exception: Programs Running", exc.Message);
                    //Trace.WriteLine(exc.ToString());
                }
                //Trace.WriteLine("Process: " + processname + " ID: " + pid + " start time: " + starttime + " cpu: " + cpu);
                if (!first)
                    output += ",";
                output += "{\"name\":\"" + processname + "\",\"id\":\"" + pid + "\",\"start_time\":\"" + starttime + "\",\"cpu\":\"" + cpu + "\"}";

                first = false;
            }
            output += "]";
            return output;
        }

        /// <summary>
        /// Check is a specifc task is in ready state
        /// </summary>
        /// <param name="taskName"> task Name "YDreams Updater"</param>
        /// <returns> string with false idf task doesn't existe or the task state otherwise</returns>
        public string TaskSchedulerCheckCall(string taskName)
        {
            //TODO: use a try catch 
            string output = "False";
            ITaskService taskService = new TaskScheduler.TaskScheduler();
            taskService.Connect();
            List<IRegisteredTask> tasks = new List<IRegisteredTask>();
            //Trace.WriteLine("getting folder");
            ITaskFolder folder = taskService.GetFolder(@"\");

            foreach (IRegisteredTask task in folder.GetTasks(1)) // get all tasks including those which are hidden, otherwise 0
            {
                //tasks.Add(task);

                //Trace.WriteLine("hello world");
                //Trace.WriteLine(task.Name + " " + task.State);
                //Console.WriteLine(task.Name + " " + task.State);
                if ((task.Name == taskName))
                {
                    output = task.State.ToString();
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(task); // release COM object
                    break;
                }

                System.Runtime.InteropServices.Marshal.ReleaseComObject(task); // release COM object
            }
            System.Runtime.InteropServices.Marshal.ReleaseComObject(folder);

            return output;
        }
        /// <summary>
        /// List all files in a specified path (JSon String)
        /// </summary>
        /// <param name="path"> the path for the file list</param>
        /// <returns>Jason structure in a string format</returns>
        public string FileListing(string path, Controller ctr)
        {
            string output = "[";

            try
            {
                string[] fileEntries = Directory.GetFiles(path);
                bool first = true;
                foreach (string fileName in fileEntries)
                {
                    //Trace.WriteLine(Path.GetFileName(fileName));
                    //Console.WriteLine(fileName);

                    if (!first) output += ",";
                    output += "\"" + Path.GetFileName(fileName) + "\"";
                    first = false;
                }
            }
            catch (Exception exc)
            {
                ctr.Report("FileListing Exception", exc.Message);
                //Trace.WriteLine(exc.ToString());
            }

            output += "]";
            return output;
        }
        /// <summary>
        /// The UpTime for the machine
        /// </summary>
        /// <returns> TimeSpan of the Up Time</returns>
        public TimeSpan GetUptime()
        {
            //TODO: Use a try catch
            ManagementObject mo = new ManagementObject(@"\\.\root\cimv2:Win32_OperatingSystem=@");
            DateTime lastBootUp = ManagementDateTimeConverter.ToDateTime(mo["LastBootUpTime"].ToString());
            return DateTime.Now.ToUniversalTime() - lastBootUp.ToUniversalTime();
        }

        /// <summary>
        /// Check if there is a host replying to pings in a specified address
        /// </summary>
        /// <param name="nameOrAddress"> Name or address of the target machine</param>
        /// <returns>True - machine pings, False - otherwise</returns>
        public bool PingHost(string nameOrAddress)
        {
            bool pingable = false;
            Ping pinger = new Ping();

            try
            {
                PingReply reply = pinger.Send(nameOrAddress);
                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException pexc)
            {
                // Discard PingExceptions and return false;
                //return false;
                Trace.WriteLine(pexc.ToString());
                pingable = false;
            }

            return pingable;
        }

        /// <summary>
        /// Telnet check        
        /// </summary>
        /// <param name="host">ethernet address</param>
        /// <param name="port">port</param>
        /// <param name="sendCommand">string to send to the switch "1"</param>
        /// <param name="expectedResponse">String to compare usualy "PC 1 Input"</param>
        /// <returns></returns>
        public bool TelnetCheckCall(string host, int port, string sendCommand, string expectedResponse)
        {
            string output = "";
            try
            {
                IPAddress[] IPs = Dns.GetHostAddresses(host);

                Socket s = new Socket(AddressFamily.InterNetwork,
                    SocketType.Stream,
                    ProtocolType.Tcp);

                //Trace.WriteLine("Establishing Telnet Connection to {0}", host);
                s.Connect(IPs[0], port);

                //Trace.WriteLine("Sending command");
                byte[] howdyBytes = Encoding.ASCII.GetBytes(sendCommand);
                s.Send(howdyBytes);
                System.Threading.Thread.Sleep(500);
                //Trace.WriteLine("Receiving reply");
                byte[] buffer = new byte[50];
                s.ReceiveTimeout = 2000;
                s.Receive(buffer);
                output = Encoding.ASCII.GetString(buffer);
                //Console.WriteLine(output);
                //Trace.WriteLine(output);
                //Console.WriteLine("Connection established");
                s.Disconnect(true);
            }
            catch (Exception exc)
            {
                //Console.WriteLine(exc.ToString());
                //Trace.WriteLine(exc.ToString());
                return false;
            }
            if (output.Contains(expectedResponse))
                return true;
            else
                return false;
        }

        /// <summary>
        /// main call to run the status checks
        /// </summary>
        public void RunChecks(Controller ctr)
        {
            //ctr.Logger.WriteLine("entered run checks");
            try
            {
                Double elapsed = ((TimeSpan)(DateTime.Now - LastActivation)).TotalMilliseconds;
                //ctr.Logger.WriteLine("comparing " + elapsed + " with " + Periodicity);
                if (elapsed > Periodicity)
                {
                    ctr.Logger.WriteLine("running checks");
                    string defaultLogPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Watchdog\\";
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(defaultLogPath + "status_" + (DateTime.Now).ToString("yyMMddHHmm") + ".json"))
                    {
                        var first = true;
                        file.WriteLine("{");
                        if (PingIP.CompareTo("") != 0)
                        {
                            file.WriteLine("\"ping_ip\":\"" + PingIP + "\"");
                            file.WriteLine(",");
                            file.WriteLine("\"ping_response\":\"" + PingHost(PingIP).ToString() + "\"");
                            first = false;
                        }
                        if (ListProgramsRunning == true)
                        {
                            if (first != true) file.WriteLine(",");
                            file.WriteLine("\"programs_running\":" + ProgramsRunning());
                            first = false;
                        }
                        if (ListDirectoryContents == true)
                        {
                            if (first != true) file.WriteLine(",");
                            file.WriteLine("\"directory_listing_path\":\"" + ListDirectoryContentsPath + "\"");
                            file.WriteLine(",");
                            file.WriteLine("\"directory_listing_response\":" + FileListing(@"" + ListDirectoryContentsPath, ctr));
                            first = false;
                        }
                        if (Uptime == true)
                        {
                            if (first != true) file.WriteLine(",");
                            file.WriteLine("\"machine_uptime\":\"" + GetUptime().ToString() + "\"");
                            first = false;
                        }
                        if (TaskSchedulerCheck == true)
                        {
                            if (first != true) file.WriteLine(",");
                            file.WriteLine("\"task_scheduler_check\":\"" + TaskSchedulerCheckName + "\"");
                            file.WriteLine(",");
                            file.WriteLine("\"task_scheduler_response\":\"" + TaskSchedulerCheckCall(TaskSchedulerCheckName) + "\"");
                            first = false;
                        }
                        if (TelnetCheck == true)
                        {
                            if (first != true) file.WriteLine(",");
                            file.WriteLine("\"telnet_test\":\"" + TelnetIP + ":23 '" + TelnetMessage + "' RSVP: '" + TelnetExpectedResponse + "'\"");
                            file.WriteLine(",");
                            file.WriteLine("\"telnet_response\":\"" + TelnetCheckCall(TelnetIP, 23, TelnetMessage, TelnetExpectedResponse).ToString() + "\"");
                            first = false;
                        }

                        file.WriteLine("}");
                    }

                    ctr.Logger.WriteLine("wrote status file");
                    LastActivation = DateTime.Now;
                }
            }
            catch (Exception exc)
            {
                ctr.Report("RunChecks Exception", exc.Message);
            }
            
        }
    }
}

