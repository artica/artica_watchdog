﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;

/// <summary>
/// The WatchDog namespace.
/// </summary>
namespace WatchDog
{
    /// <summary>
    /// Class Controller.
    /// </summary>
    [XmlType("WatchDog")]
    public class Controller : IDisposable
    {
        [DllImport("User32.dll")]
        static extern IntPtr SetForegroundWindow(IntPtr hWnd);

        [DllImport("User32.dll")]
        static extern IntPtr ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll")]
        public static extern int SetWindowPos(IntPtr hwnd, IntPtr hWndInsertAfter, int x, int y, int cx, int cy, uint wFlags);

        [DllImport("User32.dll", SetLastError = true)]
        static extern void SwitchToThisWindow(IntPtr hWnd, bool fAltTab);
       
        static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);

        private System.Threading.Timer appTimer;
        private System.Threading.Timer statusTimer;
        private System.Threading.Timer memTimer;

        /// <summary>
        /// Send Emails
        /// </summary>
        [XmlAttribute]
        public bool SendEmails = false;

        /// <summary>
        /// The mail to send information
        /// </summary>
        [XmlAttribute]
        public string ReceivingMail;

        /// <summary>
        /// The mail to send information from
        /// </summary>
        [XmlAttribute]
        public string SendingMailServer = "smtp.gmail.com";

        /// <summary>
        /// The mail to send information from
        /// </summary>
        [XmlAttribute]
        public int SendingMailPort = 587;

        /// <summary>
        /// The mail to send information from
        /// </summary>
        [XmlAttribute]
        public string SendingMailAddress;

        /// <summary>
        /// The password of the mail to send information from
        /// </summary>
        [XmlAttribute]
        public string SendingMailPassword;

        /// <summary>
        /// The idle time after restart
        /// </summary>
        [XmlAttribute]
        public long IdleTimeAfterRestart = 5000;

        /// <summary>
        /// Reboot time
        /// </summary>
        [XmlAttribute]
        public string RebootTime = "";

        /// <summary>
        /// Application Check Due Time
        /// </summary>
        [XmlAttribute]
        public int ApplicationCheckDueTime = 10000;

        /// <summary>
        /// Application Check Perio
        /// </summary>
        [XmlAttribute]
        public int ApplicationCheckPeriod = 60000;


        /// <summary>
        /// Status Check Due Time
        /// </summary>
        [XmlAttribute]
        public int StatusCheckDueTime = 60000;

        /// <summary>
        /// Status Check Period
        /// </summary>
        [XmlAttribute]
        public int StatusCheckPeriod = 5000;
        // this is the period to recheck all status, if this value is too high it will cap the individual status checks
        // each status has it's own internal periodicity which is checked against ellapsed time 

        /// <summary>
        /// Monitor Process Memory
        /// </summary>
        [XmlAttribute]
        public bool MonitorProcessMemory = false;

        /// <summary>
        /// Monitor Process Memory Due Time
        /// </summary>
        [XmlAttribute]
        public int MonitorProcessMemoryDueTime = 10000;

        /// <summary>
        /// Monitor Process Memory Period
        /// </summary>
        [XmlAttribute]
        public int MonitorProcessMemoryPeriod = 60000;

        /// <summary>
        /// The applications
        /// </summary>
        [XmlElement]
        public List<Application> Application = new List<Application>();

        /// <summary>
        /// StatusCheck
        /// </summary>
        [XmlElement]
        public List<StatusCheck> StatusCheck = new List<StatusCheck>();

        /// <summary>
        /// The context
        /// </summary>
        [XmlIgnore]
        public WatchDogForm context;

        /// <summary>
        /// The reset
        /// </summary>
        [XmlIgnore]
        private Stopwatch reset = new Stopwatch();

        /// <summary>
        /// The application monitor
        /// </summary>
        [XmlIgnore]
        private Dictionary<Application, Process> ApplicationMonitor = new Dictionary<Application, Process>();

        [XmlIgnore]
        public SmtpClient client;

        [XmlIgnore]
        public LoggingTraceListener Logger;

        /// <summary>
        /// The event log
        /// </summary>
        [XmlIgnore]
        public EventLog eventLog = new EventLog();

        /// <summary>
        /// The resetting state
        /// </summary>
        [XmlIgnore]
        private bool resettingState = false;

        [XmlIgnore]
        private Object appCheckLock = new Object();

        [XmlIgnore]
        private Object statusCheckLock = new Object();

        [XmlIgnore]
        private Object memCheckLock = new Object();

        /// <summary>
        /// Initializes a new instance of the <see cref="Controller"/> class.
        /// </summary>
        public Controller()
        {
            if (SendEmails == true) { 
                client = new SmtpClient(SendingMailServer, SendingMailPort)
                {
                    Credentials = new NetworkCredential(SendingMailAddress, SendingMailPassword),
                    EnableSsl = true
                };
            }
            string defaultLogPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Watchdog\\";
            string logPath = System.IO.Path.Combine(defaultLogPath, "WatchDogLog");
            Logger = new LoggingTraceListener(logPath);
            Logger.context =  this;
            try
            {
                Logger.ForceDirectoryCreation();
            }
            catch (Exception ex)
            {
                Logger = new LoggingTraceListener(defaultLogPath);
                Logger.context = this;
                Logger.WriteLine("Log Path '" + logPath + "' does not exist or was impossible to create. It will be changed to default : '" + defaultLogPath + "'");
                eventLog.WriteEntry("Log Path '" + logPath + "' does not exist or was impossible to create. It will be changed to default : '" + defaultLogPath + "'");
            }
            eventLog.Source = "WatchDog";
            
            appTimer = new System.Threading.Timer(AppTimer_Elapsed , this, ApplicationCheckDueTime, ApplicationCheckPeriod);

            statusTimer = new System.Threading.Timer(StatusTimer_Elapsed, this, StatusCheckDueTime, StatusCheckPeriod);

            if (MonitorProcessMemory == true) memTimer = new System.Threading.Timer(MemTimer_Elapsed, this, MonitorProcessMemoryDueTime, MonitorProcessMemoryPeriod);      
        }

        private void AppTimer_Elapsed(object sender)
        {
            Logger.WriteLine("app timer elapsed");
            lock (appCheckLock)
            {
                foreach (var app in ApplicationMonitor)
                {
                    try
                    {
                        if (app.Key != null && !app.Value.HasExited && app.Key.TopMost)
                        {
                            var ptr = FindWindow(null, app.Key.WindowName);
                            SetForegroundWindow(ptr);

                            const UInt32 SWP_NOSIZE = 0x0001;
                            const UInt32 SWP_NOMOVE = 0x0002;
                            const UInt32 SWP_SHOWWINDOW = 0x0040;
                            const UInt32 TOPMOST_FLAGS = SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW;
                            SetWindowPos(ptr, new IntPtr(-1), 0, 0, 0, 0, TOPMOST_FLAGS);
                            SwitchToThisWindow(ptr, true);
                        }
                        if (!resettingState && (app.Value.HasExited || !app.Value.Responding))
                        {
                            context.BeginInvoke((Action)(() => RestartApps()));
                            Report("App not responding", "The " + app.Key.FileName + "didn't respond correctly there was an error:");
                        }
                    }
                    catch (Exception ex)
                    {
                        Report("WindowTopMost", "The " + app.Key.FileName + "didn't started correctly there was an error:" + ex.ToString());
                    }
                }
                
            }
        }

        private void StatusTimer_Elapsed(object sender)
        {
            Logger.WriteLine("status timer elapsed");
            lock (statusCheckLock)
            {
                // check reboot time
                RebootCheckRoutine();

                // check status
                foreach (var status in StatusCheck)
                {
                    status.RunChecks(this);
                }
            }
        }

        private void MemTimer_Elapsed(object sender)
        {
            Logger.WriteLine("memory timer elapsed");
            lock (memCheckLock)
            {
                foreach (var app in ApplicationMonitor)
                {
                    var myProcess = app.Value;
                    try
                    {
                        if (!myProcess.HasExited)
                        {

                            // Refresh the current process property values.
                            myProcess.Refresh();
                            if (myProcess.WorkingSet64 > app.Key.MaximumSizeInBytes)
                                myProcess.Kill();

                            // Display current process statistics.
                            StringBuilder str = new StringBuilder();
                            str.Append(myProcess.ToString() + "\n");
                            str.Append("physical memory usage:" + myProcess.WorkingSet64.ToString() + "\n");
                            str.Append("base priority:" + myProcess.BasePriority + "\n");
                            str.Append("user processor time: " + myProcess.UserProcessorTime + "\n");
                            str.Append("PeakPagedMemorySize64 : " + myProcess.PeakPagedMemorySize64 + "\n");
                            str.Append("PeakVirtualMemorySize64 : " + myProcess.PeakVirtualMemorySize64 + "\n");
                            str.Append("PeakWorkingSet64 : " + myProcess.PeakWorkingSet64 + "\n");

                            if (myProcess.Responding)
                            {
                                str.Append("Status = Running \n");
                            }
                            else
                            {
                                str.Append("Status = Not Responding \n");
                            }
                            Logger.WriteLine("\nTask Monitor:\n" + str.ToString());
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.WriteLine("\nTask Monitor exception on :" + app.Key);
                        eventLog.WriteEntry("\nTask Monitor exception on :" + app.Key);
                    }

                }
            }
        }

        /// <summary>
        /// Creates the process.
        /// </summary>
        /// <param name="app">The application.</param>
        /// <returns>Process.</returns>
        private Process CreateProcess(Application app)
        {
            Process newProcess = new Process();

            newProcess.StartInfo.Verb = "runas";
            newProcess.StartInfo.Arguments = app.Arguments;
            newProcess.StartInfo.FileName = app.FileName;
            newProcess.StartInfo.UseShellExecute = true;
            newProcess.StartInfo.WorkingDirectory = app.WorkingDirectory;
            newProcess.StartInfo.WindowStyle = app.WindowStyle;
            //newProcess.EnableRaisingEvents = true;
            //newApplication.ApplicationProcess = newProcess;
            newProcess.Disposed += newProcess_Disposed;
            newProcess.ErrorDataReceived += newProcess_ErrorDataReceived;
            newProcess.Exited += newProcess_Exited;
            newProcess.EnableRaisingEvents = true;
            return newProcess;
        }

        /// <summary>
        /// Processes the applications.
        /// </summary>
        public void ProcessApplications()
        {
            foreach (var app in Application)
            {
                ApplicationMonitor.Add(app, CreateProcess(app));
                var oldProcesses = Process.GetProcessesByName(app.FriendlyName);
                foreach (var old in oldProcesses)
                {
                    old.Kill();
                    old.WaitForExit();
                }
            }
            StartAllApps();
        }

        /// <summary>
        /// Starts all apps.
        /// </summary>
        private void StartAllApps()
        {
            reset.Reset();
            reset.Start();
            resettingState = false; 
            foreach (var app in ApplicationMonitor)
            {
                System.Threading.Thread.Sleep(app.Key.TimeUntilProcessStart);
                try
                {
                    app.Value.Start();
                    if (app.Key.TopMost)
                    {
                        var ptr = app.Value.MainWindowHandle;
                        SetForegroundWindow(ptr);
                        ShowWindow(ptr, 3);
                        SetForegroundWindow(ptr);

                        const UInt32 SWP_NOSIZE = 0x0001;
                        const UInt32 SWP_NOMOVE = 0x0002;
                        const UInt32 SWP_SHOWWINDOW = 0x0040;
                        const UInt32 TOPMOST_FLAGS = SWP_NOMOVE | SWP_NOSIZE|SWP_SHOWWINDOW;
                        SetWindowPos(ptr, HWND_TOPMOST, 0, 0, 0, 0, TOPMOST_FLAGS);

                        //SetWindowPos(ptr, (IntPtr)(-1), 0, 0, 0, 0, 0x1);
                        SwitchToThisWindow(ptr, true);
                    }
                }
                catch (Exception ex)
                {
                    Report("StartAppError", "The " + app.Key.FileName + "didn't started correctly there was an error:" + ex.ToString());
                }
            }

        }

        /// <summary>
        /// Stops the apps.
        /// </summary>
        private void StopApps()
        {
            try
            {
                foreach (var app in ApplicationMonitor)
                {
                    if (!app.Value.HasExited)
                    {
                        app.Value.Kill();
                        app.Value.WaitForExit();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLine("\nException stopping apps :" + ex.StackTrace);
                eventLog.WriteEntry("\nException stopping apps :" + ex.StackTrace);
            }
        }

        /// <summary>
        /// Restarts the apps.
        /// </summary>
        private void RestartApps()
        {
            resettingState = true;
            StopApps();
            var remainingTime = IdleTimeAfterRestart;
            System.Threading.Thread.Sleep((int)remainingTime);
            lock (appCheckLock)
            {
                ApplicationMonitor.Clear();
                ProcessApplications();
            }
        }
        
        /// <summary>
        /// Gets the date from filename.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <returns>UpdateDate.</returns>
        static public UpdateDate GetDateFromFilename(string filename)
        {
            string str = Path.GetFileName(filename); ;
            string substr = str.Substring(12, 6);
            return new UpdateDate(substr);
        }

        /// <summary>
        /// Reports the errors.
        /// </summary>
        /// <param name="prc">The PRC.</param>
        /// <param name="filename">The filename.</param>
        private void ReportErrors(Process prc, string filename)
        {
            if (!prc.HasExited)
            {
                Report("The updater timed out", "The updater should have exited normally didn't renamed the file so it will try again tomorrow.");
                prc.Kill();
            }
            else
            {
                Report("Updater succeeded", "The updater ran have succeeded and exited normally will now rename the file.");
                if (File.Exists(filename) && !Path.GetFileName(filename).Contains("start") && !Path.GetFileName(filename).Contains("stop"))
                    File.Move(filename, Path.ChangeExtension(filename, "bex"));
            }
        }
        

        /// <summary>
        /// Check if Reboot is due
        /// </summary>
        private void RebootCheckRoutine()
        {
            Logger.WriteLine("entered reboot check routine");
            try
            {
                // check reboot time
                if (RebootTime.Equals("") != true)
                {

                    string defaultLogPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Watchdog\\";
                    string dateFilename = defaultLogPath + "rebootdate.txt";

                    bool doReboot = false;
                    if (File.Exists(dateFilename))
                    {
                        Logger.WriteLine("rebootdate file exists");
                        using (StreamReader sr = new StreamReader(dateFilename))
                        {
                            DateTime dt = DateTime.Parse(File.ReadAllText(dateFilename));

                            if (DateTime.Today > dt)
                            {
                                Logger.WriteLine("due for reboot");
                                doReboot = true;
                            }
                        }
                    }
                    else {
                        Logger.WriteLine("rebootdate file does not exist");
                        doReboot = true;
                    }

                    if (doReboot == true)
                    {
                        var time = TimeSpan.Parse(RebootTime);
                        var rebootDateTime = DateTime.Today.Add(time);
                        var currDateTime = DateTime.Now;

                        if (currDateTime > rebootDateTime)
                        {
                            using (StreamWriter newTask = new StreamWriter(dateFilename, false))
                            {
                                newTask.WriteLine(DateTime.Today);
                            }

                            Logger.WriteLine("updating reboot file and rebooting in 5 seconds");
                            Process.Start("shutdown", "/r /t 5");

                        }
                    } else
                    {
                        Logger.WriteLine("no reboot required");
                    }
                }

            }
            catch (Exception exc)
            {
                Report("WindowTopMost", "Failed to handle reboot time " + RebootTime + " :: " + exc.ToString());
            }
        }

        /// <summary>
        /// Reports the specified subject.
        /// </summary>
        /// <param name="subject">The subject.</param>
        /// <param name="message">The message.</param>
        public void Report(string subject, string message)
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback((Object obj) =>
            {
                //AEISWatchdog
                var logEntry = "Subject:[" + subject + "] " + "Message:[" + message + "]";
                try
                {
                    if (SendEmails == true)
                    {
                        client.Send(SendingMailAddress, ReceivingMail, "[" + System.Environment.MachineName + "] " + subject, message);
                    }
                    eventLog.WriteEntry(logEntry, EventLogEntryType.Warning, 12345);
                }
                catch (Exception ex)
                {
                    eventLog.WriteEntry("Error sending message: " + logEntry, EventLogEntryType.Error, 12344);
                }
            }));
        }

        /// <summary>
        /// Static Report
        /// </summary>
        /// <param name="ctr">The controller class where the reporting configuration is stored.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="message">The message.</param>
        public static void Report(Controller ctr, string subject, string message)
        {
            SmtpClient client = ctr.client;
            EventLog eventLog = ctr.eventLog;
            string ReceivingMail = ctr.ReceivingMail;
            bool SendEmails = ctr.SendEmails;
            string SendingMailAddress = ctr.SendingMailAddress;
            ThreadPool.QueueUserWorkItem(new WaitCallback((Object obj) =>
            {
                var logEntry = "Subject:[" + subject + "] " + "Message:[" + message + "]";
                try
                {
                    if (SendEmails == true)
                    {
                        client.Send(SendingMailAddress, ReceivingMail, "[" + System.Environment.MachineName + "] " + subject, message);
                    }
                    eventLog.WriteEntry(logEntry, EventLogEntryType.Warning, 12345);
                }
                catch (Exception ex)
                {
                    eventLog.WriteEntry("Error sending message: " + logEntry, EventLogEntryType.Error, 12344);
                }
            }));
        }

        #region Dispose

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                resettingState = true;
                foreach (var pair in ApplicationMonitor)
                {
                    try
                    {
                        if (!pair.Value.HasExited)
                        {
                            pair.Value.Kill();
                            pair.Value.Dispose();
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
        }

        #endregion

        #region ProcessEventsHandling

        /// <summary>
        /// Handles the Exited event of the newProcess control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        void newProcess_Exited(object sender, EventArgs e)
        {
            if (!resettingState)
            {
                Process s = (Process)sender;
                foreach (var obj in ApplicationMonitor)
                {
                    if (obj.Value.Id == s.Id)
                        Report("Exited", "Process:" + obj.Key.FriendlyName + " EventArgs:" + e.ToString());
                }
                context.Invoke((Action)(() => RestartApps()));
            }
            //RestartApps();
        }

        /// <summary>
        /// Handles the ErrorDataReceived event of the newProcess control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DataReceivedEventArgs"/> instance containing the event data.</param>
        void newProcess_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (!resettingState)
            {
                Process s = (Process)sender;
                foreach (var obj in ApplicationMonitor)
                {
                    if (obj.Value.Id == s.Id)
                        Report("ErrorDataReceived", "Process:" + obj.Key.FriendlyName + " DataReceivedEventArgs:" + e.Data);
                }
                context.Invoke((Action)(() => RestartApps()));
            }
        }

        /// <summary>
        /// Handles the Disposed event of the newProcess control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        void newProcess_Disposed(object sender, EventArgs e)
        {
            if (!resettingState)
            {
                Report("Disposed", "Process disposed EventArgs:" + e.ToString());
                context.Invoke((Action)(() => RestartApps()));
            }
        }

        #endregion

        /// <summary>
        /// Reads the XML.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns>Controller.</returns>
        public static Controller LoadXml(string path)
        {
            Controller sync;
            using (FileStream stream = new FileStream(path, FileMode.OpenOrCreate))
            {
                using (var reader = XmlReader.Create(stream))
                {
                    var xmlSerializer = new XmlSerializer(typeof(Controller));

                    sync = (Controller)xmlSerializer.Deserialize(reader);
                    //var xmlEncodedList = Encoding.UTF8.GetString(stream.ToArray());
                }
            }
            return sync;
        }

        /// <summary>
        /// Writes the XML.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="ctr">The CTR.</param>
        /*public void SaveXml(string path)
        {
            using (FileStream stream = new FileStream(path, FileMode.OpenOrCreate))
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                using (var writer = XmlWriter.Create(stream, settings))
                {
                    var xmlSerializer = new XmlSerializer(this.GetType());
                    XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                    ns.Add("", "");

                    xmlSerializer.Serialize(writer, this, ns);
                    //var xmlEncodedList = Encoding.UTF8.GetString(stream.ToArray());
                }
            }
        }*/

    }
}
