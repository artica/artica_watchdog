﻿
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

/// <summary>
/// The WatchDog namespace.
/// </summary>
namespace WatchDog
{
    /// <summary>
    /// Class StringEventArgs.
    /// </summary>
    public class StringEventArgs : EventArgs
    {
        /// <summary>
        /// The exception
        /// </summary>
        private readonly string exception;

        /// <summary>
        /// Initializes a new instance of the <see cref="StringEventArgs"/> class.
        /// </summary>
        /// <param name="ex">The ex.</param>
        public StringEventArgs(string ex)
        {
            this.exception = ex;
        }

        /// <summary>
        /// Gets the except.
        /// </summary>
        /// <value>The except.</value>
        public string Except
        {
            get { return this.exception; }
        }
    }

    /// <summary>
    /// Class UpdateDate.
    /// </summary>
    public class UpdateDate
    {
        /// <summary>
        /// The m_format
        /// </summary>
        private string m_format = "yyMMdd";

        /// <summary>
        /// Gets the date.
        /// </summary>
        /// <value>The date.</value>
        public DateTime Date { get; private set; }

        /// <summary>
        /// Gets the sr.
        /// </summary>
        /// <value>The sr.</value>
        public StreamReader sr {get; private set;}

        /// <summary>
        /// Occurs when [update exception event].
        /// </summary>
        public event EventHandler UpdateExceptionEvent;

        /// <summary>
        /// Handles the <see cref="E:UpdateException" /> event.
        /// </summary>
        /// <param name="e">The <see cref="StringEventArgs"/> instance containing the event data.</param>
        protected virtual void OnUpdateException(StringEventArgs e)
        {
            EventHandler handler = UpdateExceptionEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateDate"/> class.
        /// </summary>
        /// <param name="date">The date.</param>
        public UpdateDate(string date)
        {
            try
            {
                date = date.Replace("\r\n", string.Empty);
                Date = DateTime.ParseExact(date, m_format, CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                OnUpdateException(new StringEventArgs(ex.Message));
                Date = new DateTime();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateDate"/> class.
        /// </summary>
        /// <param name="sr">The sr.</param>
        public UpdateDate(StreamReader sr)
        {
            try
            {
                string dateStr = sr.ReadToEnd();
                dateStr = dateStr.Replace("\r\n", string.Empty);
                Date = DateTime.ParseExact(dateStr, m_format, CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                OnUpdateException(new StringEventArgs(ex.Message));
                Date = new DateTime();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateDate"/> class.
        /// </summary>
        /// <param name="datetime">The datetime.</param>
        public UpdateDate(DateTime datetime)
        {
            try
            {
                string _d = datetime.ToString(m_format);
                Date = DateTime.ParseExact(_d, m_format, CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                OnUpdateException(new StringEventArgs(ex.Message));
                Date = new DateTime();
            }
        }

        /// <summary>
        /// Implements the &gt;.
        /// </summary>
        /// <param name="d1">The d1.</param>
        /// <param name="d2">The d2.</param>
        /// <returns>The result of the operator.</returns>
        public static bool operator >(UpdateDate d1, UpdateDate d2)
        {
            return d1.Date > d2.Date;
        }

        /// <summary>
        /// Implements the &lt;.
        /// </summary>
        /// <param name="d1">The d1.</param>
        /// <param name="d2">The d2.</param>
        /// <returns>The result of the operator.</returns>
        public static bool operator <(UpdateDate d1, UpdateDate d2)
        {
            return d1.Date < d2.Date;
        }

        /// <summary>
        /// Writes the update date.
        /// </summary>
        /// <param name="sw">The sw.</param>
        public void WriteUpdateDate(StreamWriter sw)
        {
            try
            {
                string dateStr = Date.ToString(m_format);
                sw.WriteLine(dateStr);
            }
            catch (Exception ex)
            {
                OnUpdateException(new StringEventArgs(ex.Message));
            }
        }
    }
}
