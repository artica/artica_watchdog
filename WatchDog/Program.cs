﻿
using System;
using System.Threading;
using System.Windows.Forms;

/// <summary>
/// The WatchDog namespace.
/// </summary>
namespace WatchDog
{
    /// <summary>
    /// Class Program.
    /// </summary>
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            System.Windows.Forms.Application.EnableVisualStyles();
            System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);

            // Add the event handler for handling UI thread exceptions to the event.
            System.Windows.Forms.Application.ThreadException += new ThreadExceptionEventHandler(Form1_UIThreadException);

            // Set the unhanded exception mode to force all Windows Forms errors to go through 
            // our handler.
            System.Windows.Forms.Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);

            // Add the event handler for handling non-UI thread exceptions to the event. 
            AppDomain.CurrentDomain.UnhandledException +=
                new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            var form = new WatchDogForm();
            System.Windows.Forms.Application.Run(form);
        }

        /// <summary>
        /// Handles the UnhandledException event of the CurrentDomain control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="UnhandledExceptionEventArgs"/> instance containing the event data.</param>
        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            try
            {
                var ex = (Exception)e.ExceptionObject;
                var s = (WatchDogForm)sender;
                Controller.Report(s.Ctr, "Main Exception", "\nThreadExceptionEventArgs:" + ex.Message + " Inner:" + ex.InnerException.Message + "\n\nStack Trace:\n" + ex.StackTrace);
            }
            catch (Exception exc)
            {

            }
            finally
            {
                System.Windows.Forms.Application.Exit();
            }

        }

        /// <summary>
        /// Handles the UIThreadException event of the Form1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="t">The <see cref="ThreadExceptionEventArgs"/> instance containing the event data.</param>
        private static void Form1_UIThreadException(object sender, ThreadExceptionEventArgs t)
        {
            try
            {
                var ex = (Exception)t.Exception;
                var s = (WatchDogForm)sender;
                Controller.Report(s.Ctr, "Main Exception", "\nThreadExceptionEventArgs:" + ex.Message + " Inner:" + ex.InnerException.Message + "\n\nStack Trace:\n" + ex.StackTrace);
            }
            catch (Exception exc)
            {

            }
            finally
            {
                System.Windows.Forms.Application.Exit();
            }

        }
    }
}