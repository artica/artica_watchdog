﻿
using System;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;

/// <summary>
/// The WatchDog namespace.
/// </summary>
namespace WatchDog
{
    /// <summary>
    /// Class WatchDogForm.
    /// </summary>
    public partial class WatchDogForm : Form
    {

        delegate void StringArgReturningVoidDelegate(string text);

        /// <summary>
        /// The tray menu
        /// </summary>
        private ContextMenu trayMenu;

        /// <summary>
        /// The tray icon
        /// </summary>
        private NotifyIcon trayIcon;

        /// <summary>
        /// The CTR
        /// </summary>
        public Controller Ctr;

        /// <summary>
        /// Initializes a new instance of the <see cref="WatchDogForm"/> class.
        /// </summary>
        public WatchDogForm()
        {
            trayMenu = new System.Windows.Forms.ContextMenu();
            trayMenu.MenuItems.Add("Exit", OnExit);
            trayIcon = new NotifyIcon();
            trayIcon.Text = "Artica WatchDog " + System.Windows.Forms.Application.ProductVersion;
            trayIcon.Icon = new System.Drawing.Icon("artica_watchdog.ico", new Size(40, 40));
            trayIcon.DoubleClick += trayIcon_DoubleClick;
            trayIcon.ContextMenu = trayMenu;
            trayIcon.Visible = true;
           // TestWriteXml();
            string configFile = ConfigurationManager.AppSettings["ConfigFile"];
            Ctr = Controller.LoadXml(configFile);
            //Ctr.SaveXml("test.xml");
            Ctr.context = this;
            Ctr.ProcessApplications();
           // Debug.WriteLine(Ctr.MailToSendInfo);
            InitializeComponent();
        }

        /// <summary>
        /// Handles the DoubleClick event of the trayIcon control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        void trayIcon_DoubleClick(object sender, EventArgs e)
        {
            this.Visible = true;
        }

        /// <summary>
        /// Handles the <see cref="E:Exit" /> event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnExit(object sender, EventArgs e)
        {
            Ctr.Dispose();
            Process.GetCurrentProcess().Kill();
            Environment.Exit(0);
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.Load" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            BeginInvoke(new MethodInvoker(delegate
            {
                Hide();
            }));
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.FormClosing" /> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.FormClosingEventArgs" /> that contains the event data.</param>
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            BeginInvoke(new MethodInvoker(delegate
            {
                Hide();
            }));
            e.Cancel = true;
        }
        

        private void button1_Click_1(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            Process[] processlist = Process.GetProcesses();
            foreach (Process p in processlist)
            {
                listBox1.Items.Add(p.ProcessName + " (" + (p.Id).ToString() + ")");
            }
        }

        public void LogSomething(String thisstring)
        {
            if(this.logsBox.InvokeRequired)
            {
                StringArgReturningVoidDelegate d = new StringArgReturningVoidDelegate(LogSomething);
                this.Invoke(d, new object[] { thisstring });
            }  
            else  
            {
                logsBox.AppendText(thisstring + "\n");
            } 
        }
    }
}
