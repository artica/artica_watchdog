﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WatchDog
{
    public class LoggingTraceListener
        : TraceListener
    {
        private int logFiles = 7;
        private string timeFormat = "[{0:HH:mm:ss}]";

        private bool disposed;
        private TextWriter writer;
        private DateTime dateTime;

        public Controller context;

        public LoggingTraceListener()
        {

        }

        public int LogFiles
        {
            get
            {
                if (!String.IsNullOrEmpty(this.Attributes["logFiles"]))
                {
                    logFiles = Int32.Parse(this.Attributes["logFiles"]);
                }
                return logFiles;
            }
            set
            {
                logFiles = value;
            }
        }

        public string TimeFormat
        {
            get
            {
                if (!String.IsNullOrEmpty(this.Attributes["timeFormat"]))
                {
                    timeFormat = this.Attributes["timeFormat"];
                }
                return timeFormat;
            }

            set
            {
                timeFormat = value;
            }
        }

        public LoggingTraceListener(string baseName)
            : this(baseName, null)
        {
        }

        public LoggingTraceListener(string baseName, string name)
            : base(name)
        {
            if (string.IsNullOrEmpty(baseName))
            {
                throw new ArgumentNullException("baseName");
            }

            BaseName = baseName;
        }

        protected override string[] GetSupportedAttributes()
        {
            return new string[] { "logFiles", "timeFormat" };
        }


        public string BaseName { get; set; }

        public override void Flush()
        {
            try
            {
                if (EnsureWriter(DateTime.Now))
                {
                    writer.Flush();
                }
            }
            catch
            { }
        }

        public override void Write(string message)
        {
            var now = DateTime.Now;
            if (EnsureWriter(now))
            {
                if (NeedIndent)
                {
                    WriteIndent();
                    writer.Write(TimeFormat, now);
                }
                writer.Write(message);

                context.context.LogSomething(now.ToString("HH:mm:ss") + " " + message);
            }
            Flush();
        }

        public override void WriteLine(string message)
        {
            try
            {
                var now = DateTime.Now;
                if (EnsureWriter(now))
                {
                    if (NeedIndent)
                    {
                        WriteIndent();
                        writer.Write(TimeFormat, now);
                    }
                    writer.WriteLine(message);
                    NeedIndent = true;
                }
                Flush();

                context.context.LogSomething(now.ToString("HH:mm:ss") + " " + message);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private bool EnsureWriter(DateTime time)
        {
            var flag = true;
            if (dateTime.Date != time.Date)
            {
                flag = false;
                if (writer != null) writer.Close();
                if (string.IsNullOrEmpty(BaseName)) return flag;

                CreateDirectory(BaseName);
                var fileName = GetFileName(BaseName, time);
                try
                {
                    writer = new StreamWriter(fileName, true);
                    flag = true;
                    dateTime = time;
                    Flush();
                }
                catch (IOException)
                {
                    // May be in use; try again, different name
                    fileName = GetFileName(BaseName, time, Guid.NewGuid());
                    flag = false;
                }
            }

            return flag;
        }

        public string ForceDirectoryCreation()
        {
            return CreateDirectory(BaseName);
        }

        private string CreateDirectory(string baseName)
        {
            var directoryName = Path.GetDirectoryName(baseName);

            Directory.CreateDirectory(directoryName);

            var files = GetFileNames(baseName);
            var count = files.Length - LogFiles;

            if (0 < count) Array.Sort(files);
            for (int i = 0; i < count; i++)
            {
                try
                {
                    File.Delete(files[i]);
                }
                catch
                {
                    // May be in use; continue
                }
            }

            return directoryName;
        }

        private static string[] GetFileNames(string baseName)
        {
            return Directory.GetFiles(
                Path.GetDirectoryName(baseName),
                Path.GetFileNameWithoutExtension(baseName) + "*" + Path.GetExtension(baseName));
        }

        private static string GetFileName(string baseName, DateTime date)
        {
            return string.Format(@"{0}\{1}-{3:yyyy.MM.dd}{2}",
                Path.GetDirectoryName(baseName),
                Path.GetFileNameWithoutExtension(baseName),
                Path.GetExtension(baseName),
                date);
        }

        private static string GetFileName(string baseName, DateTime date, Guid guid)
        {
            return string.Format(@"{0}\{1}-{3:yyyy.MM.dd}-{4}{2}",
                Path.GetDirectoryName(baseName),
                Path.GetFileNameWithoutExtension(baseName),
                Path.GetExtension(baseName),
                date, guid);
        }

        #region Dispose Pattern

        public override void Close()
        {
            Dispose();
        }

        protected override void Dispose(bool disposing)
        {
            try
            {
                if (!disposed)
                {
                    if (disposing)
                    {
                        if (writer != null)
                        {
                            writer.Close();
                        }

                        writer = null;
                        disposed = true;
                    }
                }
            }
            finally { base.Dispose(disposing); }
        }

        #endregion
    }
}
